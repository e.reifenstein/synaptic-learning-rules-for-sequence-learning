This repo contains code and data for the study 'Synaptic learning rules for sequence learning', published in eLife 2021.
Cite as: eLife 2021;10:e67171 doi: 10.7554/eLife.67171
https://elifesciences.org/articles/67171
