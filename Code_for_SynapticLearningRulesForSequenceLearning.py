"""
This code simulates inhomogenuous Poisson processes as described
in Reifenstein and Kempter (eLife, 2021) and generates the corresponding plots.
"""

#%%
import numpy as np
import numpy.random as rand
from scipy.signal import fftconvolve
import matplotlib.pyplot as plt
pi = np.pi

max_t = 10.0
delta = 0.001
sigma = 0.3 # s, place-field width, = L/(3*sqrt(2))
omega = 2*pi*10 # Hz, cell-intrinsic frequency
A = 10 # mean firing rate
omega_ref = omega/1.04
c = 0.042

# rate functions with and without theta modulation
rate_function = (lambda t, T, sigma, A, c: A*np.exp(-((t - T)**2)/2./sigma**2)\
                 /np.sqrt(2*pi)/sigma * (1 + np.cos(omega*(t - c*T))))
rate_function2 = (lambda t, T, sigma, A, c: A*np.exp(-(t - T)**2/2./sigma**2)\
                  /np.sqrt(2*pi)/sigma)

def generate_times_opt(rate_function, T, omega_ref, sigma, A, c, max_t, delta):
    '''
    create a realization of an inhomogenuous Poisson process with rate 'rate_function'
    '''
    t = np.arange(delta, max_t, delta)
    ph = np.mod(180/pi*omega_ref*t, 360)
    avg_rate = (rate_function(t, T, sigma, A, c) + rate_function(t + delta, T, sigma, A, c)) / 2.0
    avg_prob = 1 - np.exp(-avg_rate * delta)
    rand_throws = rand.uniform(size=t.shape[0])
    return t[avg_prob >= rand_throws], ph[avg_prob >= rand_throws]

def raster(event_times_list, color='k'):
    """
    creates raster plot
    """
    ax = plt.gca()
    for ith, trial in enumerate(event_times_list):
        plt.vlines(trial, ith + .5, ith + 1.5, color=color)
    plt.ylim(.5, len(event_times_list) + .5)
    return ax

def overall_w_change(cell1, cell2, tau=0.01, window='odd', normed=False):
    '''
    calculates the overall weight change for the STDP rule specified in 'window'
    '''
    if len(cell1) == 0 or len(cell2) == 0:
        return 0

    if window == 'triplet':
        # Pfister and Gerstner, 2006
        # parameters from the hippocampal data set (Wang et al., Nat Neuro, 2005),
        # All-to-All full model
        dt = 0.001
        tmax = max(cell1[-1], cell2[-1]) + 0.5
        timestamp = np.arange(dt, tmax+dt, dt)
        cell1_spk = np.sum(np.array(list(map(lambda x: np.where(timestamp == x, 1, 0),
                                             cell1))), axis=0) #create spike train
        cell2_spk = np.sum(np.array(list(map(lambda x: np.where(timestamp == x, 1, 0),
                                             cell2))), axis=0)
        tau_plus = 0.0168 # 0.01
        tau_minus = 0.0337 # 0.01
        tau_x = 0.1 #0.946 # 0.01
        tau_y = 0.04 #0.946 #0.027 # 0.01
        A2_plus = 5.3e-3 #0.777 #6.1e-3
        A2_minus = 3.5e-3 #0.237 #1.6e-3
        A3_plus = 8e-3 #0 #6.7e-3
        A3_minus = 0 # 1.4e-3

        thelp = np.arange(0, 5*tau_x, dt)
        r1 = np.roll(list(map(lambda x: max(x, 0),
                              fftconvolve(cell1_spk, np.exp(-thelp/tau_plus), 'same'))),
                     int(len(thelp)/2))
        r2 = np.roll(list(map(lambda x: max(x, 0),
                              fftconvolve(cell1_spk, np.exp(-thelp/tau_x), 'same'))),
                     int(len(thelp)/2))
        o1 = np.roll(list(map(lambda x: max(x, 0),
                              fftconvolve(cell2_spk, np.exp(-thelp/tau_minus), 'same'))),
                     int(len(thelp)/2))
        o2 = np.roll(list(map(lambda x: max(x, 0),
                              fftconvolve(cell2_spk, np.exp(-thelp/tau_y), 'same'))),
                     int(len(thelp)/2))
        dw = 0
        dws = np.zeros(len(timestamp))
        for it, tt in enumerate(timestamp):
            if tt in cell1:
                dw -= o1[it]*(A2_minus + A3_minus*r2[it-1])
            if tt in cell2:
                dw += r1[it]*(A2_plus + A3_plus*o2[it-1])
            dws[it] = dw
    else:
        def bittner_window(t, tau):
            if t >= 0:
                return np.exp(-t/tau/1.3)
            return np.exp(t/tau/0.7)

        def bittner_norm(t, tau):
            if t >= 0:
                return 10/3.*np.exp(-t/tau/1.3)
            return 10/3.*np.exp(t/tau/0.7)

        def bipoo_window(t, tau):
            return np.where(t >= 0, 0.777*np.exp(-t/0.0168), -0.273*np.exp(t/0.0337))

        def bipoo_even(t, tau):
            return 0.5*(bipoo_window(t, tau) + bipoo_window(-t, tau))

        def bipoo_odd(t, tau):
            return 0.5*(bipoo_window(t, tau) - bipoo_window(-t, tau))

        def wittenberg06(t, tau):
            return 75*(np.exp(-1./128 *  (t - 10)**2)/(8*np.sqrt(2*pi))
                       - np.exp(-1./800 *  (t-12)**2)/(20*np.sqrt(2*pi)))

        def froemke05(t, tau):
            return np.where(t >= 0, 0.36*np.exp(-t/0.0125), -0.28*np.exp(t/0.1034))

        if window == 'even':
            syn_window = lambda t, tau: np.exp(-abs(t)/tau)
        elif window == 'odd':
            syn_window = lambda t, tau: np.sign(t)*np.exp(-abs(t)/tau)
        elif window == 'bittner':
            syn_window = bittner_window
        elif window == 'bittner_norm':
            syn_window = bittner_norm
        elif window == 'bipoo':
            syn_window = bipoo_window
        elif window == 'wittenberg06':
            syn_window = wittenberg06
        elif window == 'bipoo_even':
            syn_window = bipoo_even
        elif window == 'bipoo_odd':
            syn_window = bipoo_odd
        elif window == 'froemke05':
            syn_window = froemke05
        # time in seconds!
        # loop over spikes of cell 2 (postsynaptic) and cumulate synaptic changes
        # according to the synaptic window and spike times of cell 1;
        # for tau=10ms, 50ms ahead and back should be sufficient; using 5*tau in general
        dw = 0
        for c2 in cell2:
            for c1 in cell1[(c2-5*tau < cell1)*(cell1 < c2+5*tau)]: # 5*tau look around
                dw += syn_window(c2-c1, tau)
        if normed:
            try:
                dw = dw/len(cell1)/len(cell2)
            except ZeroDivisionError:
                dw = 0
    return dw

#%%
# raster plot of many realizations
ntrials = 1000
spikes = []
T = 5 # field center
for i in range(ntrials):
    spikes.append(generate_times_opt(rate_function, T, omega_ref, sigma, 10, c, max_t, delta)[0])

plt.figure()
ax = raster(spikes)
plt.xlabel('time (s)')
plt.ylabel('trial')

#%%
# Spike trains for Figure 1
for ii in range(10):
    spikes = []
    spikes.append(generate_times_opt(rate_function, 1.3, omega_ref, 0.2, 10, c, max_t, delta)[0])
    spikes.append(generate_times_opt(rate_function, 0.9, omega_ref, 0.2, 10, c, max_t, delta)[0])
    spikes.append(generate_times_opt(rate_function, 0.8, omega_ref, 0.2, 10, c, max_t, delta)[0])
    spikes.append(generate_times_opt(rate_function, 0.4, omega_ref, 0.2, 10, c, max_t, delta)[0])
    plt.figure()
    ax = raster(spikes)
    plt.xlabel('time (s)')
    plt.ylabel('trial')

#%%
# Simulations for Figure 3 - runs quite long
max_t = 10.0
delta = 0.001
dwpp_mean = []
dwpl_mean = []
dwno_mean = []
dwpp_std = []
dwpl_std = []
dwno_std = []

dwppback_mean = []
dwplback_mean = []
dwnoback_mean = []
dwppback_std = []
dwplback_std = []
dwnoback_std = []

shifts = np.arange(0, 2.01, 0.1) #0.05)
reps = 10000
A = 10 # 100
c = 0.042
omega_ref = omega/1.014
symmetric = False
normed = False
window = 'triplet'
for shift in shifts:
    print(shift)
    dw_pp = []
    dw_pl = []
    dw_no = []
    dw_pp_back = []
    dw_pl_back = []
    dw_no_back = []
    for i in range(reps):
        spkt1, ph1 = generate_times_opt(rate_function, 3, omega_ref, sigma, A, c, max_t, delta)
        spkt2, ph2 = generate_times_opt(rate_function, 3 + shift, omega_ref, sigma,
                                        A, c, max_t, delta)
        dw_pp.append(overall_w_change(spkt1, spkt2, window=window, tau=0.01, normed=normed))
        dw_pp_back.append(overall_w_change(spkt2, spkt1, window=window, tau=0.01, normed=normed))
        spkt1, ph1 = generate_times_opt(rate_function, 3, omega, sigma, A, 0, max_t, delta)
        spkt2, ph2 = generate_times_opt(rate_function, 3 + shift, omega, sigma, A, 0, max_t, delta)
        dw_pl.append(overall_w_change(spkt1, spkt2, window=window, tau=0.01, normed=normed))
        dw_pl_back.append(overall_w_change(spkt2, spkt1, window=window, normed=normed))
        spkt1, ph1 = generate_times_opt(rate_function2, 3, omega, sigma, A, None, max_t, delta)
        spkt2, ph2 = generate_times_opt(rate_function2, 3 + shift, omega, sigma,
                                        A, None, max_t, delta)
        dw_no.append(overall_w_change(spkt1, spkt2, window=window, tau=0.01, normed=normed))
        dw_no_back.append(overall_w_change(spkt2, spkt1, window=window, normed=normed))
    dwpp_mean.append(np.mean(np.array(dw_pp)))
    dwpl_mean.append(np.mean(np.array(dw_pl)))
    dwno_mean.append(np.mean(np.array(dw_no)))
    dwppback_mean.append(np.mean(np.array(dw_pp_back)))
    dwplback_mean.append(np.mean(np.array(dw_pl_back)))
    dwnoback_mean.append(np.mean(np.array(dw_no_back)))
    dwpp_std.append(np.std(np.array(dw_pp)))
    dwpl_std.append(np.std(np.array(dw_pl)))
    dwno_std.append(np.std(np.array(dw_no)))
    dwppback_std.append(np.std(np.array(dw_pp_back)))
    dwplback_std.append(np.std(np.array(dw_pl_back)))
    dwnoback_std.append(np.std(np.array(dw_no_back)))

plt.figure(figsize=(6, 6))
plt.subplot(3, 1, 1)
#plt.scatter(shifts, np.array(dwpp_mean), color='blue')
#plt.scatter(shifts, np.array(dwpl_mean), color='red')
#lt.scatter(shifts, np.array(dwno_mean), color='green')
plt.scatter(shifts, np.array(dwpp_mean)-np.array(dwppback_mean), color='blue')
plt.scatter(shifts, np.array(dwpl_mean)-np.array(dwplback_mean), color='red')
plt.scatter(shifts, np.array(dwno_mean)-np.array(dwnoback_mean), color='green')
plt.legend(['Phase precession', 'Phase locking', 'No theta'])
plt.axhline(0, 0, 1.5, color='black')
plt.axvline(sigma, color='black')
plt.axvline(3*np.sqrt(2)*sigma, color='black')
#axvline(sqrt(4/3.)*sigma,color='black')
#xlabel('time lag Tij (s)')
plt.ylabel('delta wij')
plt.xlim([0, 1.5])
plt.xticks(np.arange(0, 1.6, 0.5), [])

plt.subplot(3, 1, 2)
plt.scatter(shifts, (np.array(dwpp_mean)-np.array(dwppback_mean))\
            /(np.array(dwpl_mean)-np.array(dwplback_mean))-1, color='black')
plt.axhline(0, 0, 1.5, color='black')
plt.axvline(sigma, color='black')
plt.axvline(3*np.sqrt(2)*sigma, color='black')
#axvline(sqrt(4/3.)*sigma, color='black')
#xlabel('Firing-field separation (s)')
plt.ylabel('benefit')
plt.xlim([0, 1.5])
plt.ylim([-6, 14])
plt.xticks(np.arange(0, 1.6, 0.5), [])

plt.subplot(3, 1, 3)
plt.plot(shifts, (np.array(dwpp_mean)-np.array(dwppback_mean))\
         /(np.array(dwpp_std)+np.array(dwppback_std)), color='b')
plt.plot(shifts, (np.array(dwpl_mean)-np.array(dwplback_mean))\
         /(np.array(dwpl_std)+np.array(dwplback_std)), color='r')
plt.plot(shifts, (np.array(dwno_mean)-np.array(dwnoback_mean))\
         /(np.array(dwno_std)+np.array(dwnoback_std)), color='g')
#plot(shifts, ones(len(shifts)), 'k--')
plt.axvline(sigma, color='black')
plt.axvline(3*np.sqrt(2)*sigma, color='black')
#axvline(sqrt(4/3.)*sigma, color='black')
plt.xlabel('time lag Tij (s)')
plt.ylabel('SNR')
plt.xticks(np.arange(0, 1.6, 0.5))
plt.xlim([0, 1.5])

#%%
dwpp_mean_bipoo = dwpp_mean.copy()
dwpl_mean_bipoo = dwpl_mean.copy()
dwno_mean_bipoo = dwno_mean.copy()
dwppback_mean_bipoo = dwppback_mean.copy()
dwplback_mean_bipoo = dwplback_mean.copy()
dwnoback_mean_bipoo = dwnoback_mean.copy()
dwpp_std_bipoo = dwpp_std.copy()
dwpl_std_bipoo = dwpl_std.copy()
dwno_std_bipoo = dwno_std.copy()
dwppback_std_bipoo = dwppback_std.copy()
dwplback_std_bipoo = dwplback_std.copy()
dwnoback_std_bipoo = dwnoback_std.copy()

# Figure for revision, comparison of Bi and Poo rule with Gerstner triplet rule
plt.figure(figsize=(12, 12))
plt.subplot(3, 1, 1)
#plt.scatter(shifts, np.array(dwpp_mean), color='blue')
#plt.scatter(shifts, np.array(dwpl_mean), color='red')
#lt.scatter(shifts, np.array(dwno_mean), color='green')
plt.plot(shifts, (np.array(dwpp_mean_bipoo)-np.array(dwppback_mean_bipoo))\
         /max(np.array(dwpl_mean_bipoo)-np.array(dwplback_mean_bipoo)), 'bo', markersize=12)
plt.plot(shifts, (np.array(dwpl_mean_bipoo)-np.array(dwplback_mean_bipoo))\
         /max(np.array(dwpl_mean_bipoo)-np.array(dwplback_mean_bipoo)), 'ro', markersize=12)
plt.plot(shifts, (np.array(dwpp_mean)-np.array(dwppback_mean))\
         /max((np.array(dwpl_mean)-np.array(dwplback_mean))), 'bs', markersize=12)
plt.plot(shifts, (np.array(dwpl_mean)-np.array(dwplback_mean))\
         /max(np.array(dwpl_mean)-np.array(dwplback_mean)), 'rs', markersize=12)
#plt.scatter(shifts, np.array(dwno_mean)-np.array(dwnoback_mean), color='green')
plt.legend(['Bi and Poo, phase precession', 'Bi and Poo, phase locking',
            'Triplet minimal model, phase precession',
            'Triplet minimal model, phase locking'])
plt.axhline(0, 0, 1.5, color='black')
plt.axvline(sigma, color='black')
plt.axvline(3*np.sqrt(2)*sigma, color='black')
#axvline(sqrt(4/3.)*sigma, color='black')
#xlabel('time lag Tij (s)')
plt.ylabel('Normalized \n weight change', fontsize=20)
plt.xlim([0, 1.5])
plt.xticks(np.arange(0, 1.6, 0.5), [])
plt.yticks(fontsize=20)

plt.subplot(3, 1, 2)
plt.plot(shifts, (np.array(dwpp_mean_bipoo)-np.array(dwppback_mean_bipoo))\
         /(np.array(dwpl_mean_bipoo)-np.array(dwplback_mean_bipoo))-1, 'ko', markersize=12)
plt.plot(shifts, (np.array(dwpp_mean)-np.array(dwppback_mean))\
         /(np.array(dwpl_mean)-np.array(dwplback_mean))-1, 'ks', markersize=12)
plt.axhline(0, 0, 1.5, color='black')
plt.axvline(sigma, color='black')
plt.axvline(3*np.sqrt(2)*sigma, color='black')
#axvline(sqrt(4/3.)*sigma, color='black')
#xlabel('Firing-field separation (s)')
plt.ylabel('Benefit', fontsize=20)
plt.legend(['Bi and Poo', 'Triplet minimal model'], loc=1)
plt.xlim([0, 1.5])
plt.ylim([-1.8, 8])
plt.xticks(np.arange(0, 1.6, 0.5), [])
plt.yticks(fontsize=20)

plt.subplot(3, 1, 3)
plt.plot(shifts, (np.array(dwpp_mean_bipoo)-np.array(dwppback_mean_bipoo))\
         /(np.array(dwpp_std_bipoo)+np.array(dwppback_std_bipoo)), 'bo', markersize=12)
plt.plot(shifts, (np.array(dwpl_mean_bipoo)-np.array(dwplback_mean_bipoo))\
         /(np.array(dwpl_std_bipoo)+np.array(dwplback_std_bipoo)), 'ro', markersize=12)
plt.plot(shifts, (np.array(dwpp_mean)-np.array(dwppback_mean))\
         /(np.array(dwpp_std)+np.array(dwppback_std)), 'bs', markersize=12)
plt.plot(shifts, (np.array(dwpl_mean)-np.array(dwplback_mean))\
         /(np.array(dwpl_std)+np.array(dwplback_std)), 'rs', markersize=12)
#plt.plot(shifts,(np.array(dwno_mean)-np.array(dwnoback_mean))\
#    /(np.array(dwno_std)+np.array(dwnoback_std)), color='g')
#plot(shifts,ones(len(shifts)), 'k--')
plt.axvline(sigma, color='black')
plt.axvline(3*np.sqrt(2)*sigma, color='black')
#axvline(sqrt(4/3.)*sigma, color='black')
plt.xlabel('Field separation (s)', fontsize=20)
plt.ylabel('SNR', fontsize=20)
plt.legend(['Bi and Poo, phase precession', 'Bi and Poo, phase locking',
            'Triplet minimal model, phase precession',
            'Triplet minimal model, phase locking'])
plt.xticks(np.arange(0, 1.6, 0.5), fontsize=20)
plt.yticks(fontsize=20)
plt.xlim([0, 1.5])

#%%
# add analytical curves for Figure 3
G = lambda m, s, t: 1/(np.sqrt(2*pi)*s) * np.exp(-(t-m)**2/(2*s**2))
sigma = 0.3 # s, place-field width, L/(3*sqrt(2))
omega = 2*pi*10 # Hz, cell-intrinsic frequency
c = 0.039
tau = 0.01
Ts = np.arange(0, 1.5, 0.001)
A = 10
mu = 1
dw_all = (A**2*mu*tau**2*Ts)/sigma**2 * G(Ts, np.sqrt(2)*sigma, 0)\
    * (1 + (omega**2*sigma**2*c)/(omega**2*tau**2 + 1) * (np.sin(omega*c*Ts))/(omega*c*Ts))\
        + (1-omega**2*tau**2)*np.cos(omega*c*Ts)/(2*(1+ omega**2*tau**2)**2)
dw_c0 = (A**2*mu*tau**2*Ts)/sigma**2 * G(Ts, np.sqrt(2)*sigma, 0)\
    * (1 + (1-omega**2*tau**2)/(2*(1+ omega**2*tau**2)**2))

plt.figure(figsize=(6, 6))
plt.subplot(3, 1, 1)
plt.plot(Ts, dw_all, lw=2, color='blue')
plt.plot(Ts, dw_c0, lw=2, color='red')
plt.scatter(shifts, A**2*np.array(dwpp_mean), color='blue')
plt.scatter(shifts, A**2*np.array(dwpl_mean), color='red')
plt.axhline(0, 0, 1.5, color='black')
plt.axvline(sigma, color='black')
plt.axvline(3*np.sqrt(2)*sigma, color='black')
#axvline(sqrt(4/3.)*sigma, color='black')
#xlabel('time lag Tij (s)')
plt.ylabel('delta wij')
plt.xlim([0, 1.5])
plt.xticks(np.arange(0, 1.6, 0.5), [])

plt.subplot(3, 1, 2)
plt.plot(Ts, (Ts*(tau**2*omega**2 - 1) + (Ts - Ts*tau**2*omega**2)*np.cos(c*Ts*omega)\
              + 2*sigma**2*omega*(1 + tau**2*omega**2)*np.sin(c*Ts*omega))\
         /(Ts*(3 + 3*tau**2*omega**2 + 2*tau**4*omega**4)), color='black')
plt.plot(Ts, 2./3*omega**2*sigma**2*c\
         *(1 - (c/4/sigma**2*(1-omega**2*tau**2)/(1+omega**2*tau**2)\
                + 1./6*omega**2*c**2)* Ts**2), 'k--')
plt.scatter(shifts, np.array(dwpp_mean)/np.array(dwpl_mean)-1, color='black')
plt.axhline(0, 0, 1.5, color='black')
plt.axvline(sigma, color='black')
plt.axvline(3*np.sqrt(2)*sigma, color='black')
#axvline(sqrt(4/3.)*sigma, color='black')
#xlabel('Firing-field separation (s)')
plt.ylabel('benefit')
plt.xlim([0, 1.5])
plt.ylim([-6, 12])
plt.xticks(np.arange(0, 1.6, 0.5), [])

plt.subplot(3, 1, 3)
plt.plot(shifts, np.array(dwpp_mean)/np.array(dwpp_std), color='b')
plt.plot(shifts, np.array(dwpl_mean)/np.array(dwpl_std), color='r')
plt.plot(shifts, np.ones(len(shifts)), 'k--')
plt.plot(Ts, 3./(np.sqrt(2*pi)*np.math.erf(3./2))\
         *(A*tau**1.5*np.exp(-Ts**2/(4*sigma**2)))\
             /(sigma*np.sqrt(3*np.sqrt(2)*sigma - Ts))\
                 *(Ts/sigma - Ts*(tau**2*omega**2 - 1)\
                   *np.cos(omega*c*Ts)/(2*sigma*(1+tau**2*omega**2)**2)\
                      +omega*sigma*np.sin(omega*c*Ts)/(1+tau**2*omega**2)), 'g')
plt.axvline(sigma, color='black')
plt.axvline(3*np.sqrt(2)*sigma, color='black')
#axvline(sqrt(4/3.)*sigma, color='black')
plt.xlabel('time lag Tij (s)')
plt.ylabel('SNR')
plt.xticks(np.arange(0, 1.6, 0.5))
plt.xlim([0, 1.5])

#%%
# Simulations for Figure 5 (for long STDP window)
delta = 0.001
dwpp_mean = []
dwpl_mean = []
dwpp_std = []
dwpl_std = []

#dwppback_mean = []
#dwplback_mean = []
#dwppback_std = []
#dwplback_std = []
shifts = np.arange(0, 8.01, 0.05)
reps = 10000
A = 10
c = 0.039
sigma = 0.3
omega = 2*pi*10
omega_ref = omega/1.014
normed = False
for shift in shifts:
    print(shift)
    max_t = 25 + shift # 5*tau + shift
    dw_pp = []
    dw_pl = []
    #dw_pp_back = []
    #dw_pl_back = []
    for i in range(reps):
        spkt1, ph1 = generate_times_opt(rate_function, 2, omega_ref, sigma, A, c, max_t, delta)
        spkt2, ph2 = generate_times_opt(rate_function, 2 + shift, omega_ref, sigma,
                                        A, c, max_t, delta)
        dw_pp.append(overall_w_change(spkt1, spkt2, tau=5., window='odd', normed=normed))
        #dw_pp_back.append(overall_w_change(spkt2, spkt1, tau=5., window='odd', normed=normed))
        spkt1, ph1 = generate_times_opt(rate_function, 2, omega, sigma, A, 0, max_t, delta)
        spkt2, ph2 = generate_times_opt(rate_function, 2 + shift, omega, sigma,
                                        A, 0, max_t, delta)
        dw_pl.append(overall_w_change(spkt1, spkt2, tau=5., window='odd', normed=normed))
        #dw_pl_back.append(overall_w_change(spkt2, spkt1, sym=symmetric, normed=normed))
    dwpp_mean.append(np.mean(np.array(dw_pp)))
    dwpl_mean.append(np.mean(np.array(dw_pl)))
    #dwppback_mean.append(mean(array(dw_pp_back)))
    #dwplback_mean.append(mean(array(dw_pl_back)))
    dwpp_std.append(np.std(np.array(dw_pp)))
    dwpl_std.append(np.std(np.array(dw_pl)))
    #dwppback_std.append(std(array(dw_pp_back)))
    #dwplback_std.append(std(array(dw_pl_back)))

A = 10
L = 3*np.sqrt(2)*sigma
font = 20
linew = 3
t = np.arange(-2, 8, 0.01)
dt = 0.0001
tt = np.arange(0, 10, dt)
tcc = np.append(-tt[::-1], tt[1:])
shift = 6
#cell1 = rate_function(tt, 2, sigma, 1, c)
#cell11 = rate_function(tt, 2, sigma, 1, 0)
#cell12 = rate_function2(tt, 2, sigma, 1, None)
#cell2 = rate_function(tt, 2 + shift, sigma, 1, c)
#cell21 = rate_function(tt, 2 + shift, sigma, 1, 0)
#cell22 = rate_function2(tt, 2 + shift, sigma, 1,None)
#cc_pp = dt*correlate(cell2, cell1, 'full')
#cc_pl = dt*correlate(cell21, cell11, 'full')
#cc_no = dt*correlate(cell22, cell12, 'full')
plt.figure(figsize=(18, 12))
plt.subplot(5, 1, 1)
#plot(t,rate_function(t, 0, sigma, A, c), 'c', lw=linew)
plt.plot(t, rate_function2(t, 0, sigma, A, c), 'c', lw=linew, dashes=[20, 10])
#plot(t, rate_function(t, shift, sigma, A, c), 'b', lw=linew)
plt.plot(t, rate_function2(t, shift, sigma, A, c), 'b', lw=linew, dashes=[20, 10])
plt.xlabel('time (s)', fontsize=font)
plt.ylabel('firing rate (1/s)', fontsize=font)
plt.xticks(fontsize=font)
plt.yticks(fontsize=font)
#plt.xlim([-0.7, 1])

plt.subplot(5, 1, 2)
#plt.plot(tcc, cc_pp, 'r', lw=linew)
plt.plot(t, rate_function2(t, shift, np.sqrt(2)*sigma, 1, c), 'r', lw=linew, dashes=[20, 10])
#plt.axvline(0, 0, 1, color='k')
#plt.axvspan(-0.02, 0.02, alpha=0.3, color='k')
plt.xlim([-2, 8])
plt.xticks(fontsize=font)
plt.yticks(fontsize=font)
plt.xlabel('time lag (s)', fontsize=font)
plt.ylabel('Cij (1/s)', fontsize=font)

plt.subplot(5, 1, 3)
tau = 5
syn_window = lambda t, tau: np.sign(t)*np.exp(-abs(t)/tau)
plt.plot(t, syn_window(t, tau), 'k', lw=linew)
#plt.axvspan(-0.02, 0.02, alpha=0.3, color='k')
plt.xlabel('time lag (s)', fontsize=font)
plt.ylabel('Wodd', fontsize=font)
plt.xticks(fontsize=font)
plt.yticks(fontsize=font)
plt.tight_layout()

plt.subplot(5, 1, 4)
plt.plot(shifts, dwpp_mean, 'b-')
plt.plot(shifts, np.array(dwpp_mean)+np.array(dwpp_std), 'b--')
plt.plot(shifts, np.array(dwpp_mean)-np.array(dwpp_std), 'b--')
#plt.plot(shifts, dwpl_mean, 'r-')
#plt.plot(shifts, array(dwpl_mean)+array(dwpl_std), 'r--')
#plt.plot(shifts, array(dwpl_mean)-array(dwpl_std), 'r--')
#plt.errorbar(shifts, dwno_mean, yerr=array(dwno_std)/sqrt(reps))
#plt.legend(['Phase precession', 'Phase locking', 'No theta modulation'])
plt.xlabel('Firing-field separation (s)', fontsize=20)
plt.ylabel('dwij', fontsize=20)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
#plt.xlim([0, 1.5])
#plt.ylim([-0.0005, 0.003])

plt.subplot(5, 1, 5)
plt.errorbar(shifts, np.array(dwpp_mean)/np.array(dwpp_std))
#errorbar(shifts, array(dwpl_mean)/array(dwpl_std))
#errorbar(shifts, array(dwno_mean)/array(dwno_std))
plt.plot(shifts, np.ones(len(shifts)), 'k--')
plt.plot(shifts, A/np.sqrt(2*A+1)*np.ones(len(shifts)), 'k--')
#legend(['Phase precession','Phase locking'])#,'No theta modulation'])
plt.xlabel('time lag (s)', fontsize=20)
plt.ylabel('SNR', fontsize=20)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
#xlim([0,1.5])

#%%
# Simulations for Figure 4: SNR as a function of tau
max_t = 20.0
delta = 0.001
dwdppodd_mean = []
dwdppbitt_mean = []
dwdppodd_std = []
dwdppbitt_std = []
dwdppodd_mean_back = []
dwdppbitt_mean_back = []
dwdppodd_std_back = []
dwdppbitt_std_back = []

dwdplodd_mean = []
dwdplbitt_mean = []
dwdplodd_std = []
dwdplbitt_std = []
dwdplodd_mean_back = []
dwdplbitt_mean_back = []
dwdplodd_std_back = []
dwdplbitt_std_back = []

dwdnoodd_mean = []
dwdnobitt_mean = []
dwdnoodd_std = []
dwdnobitt_std = []
dwdnoodd_mean_back = []
dwdnobitt_mean_back = []
dwdnoodd_std_back = []
dwdnobitt_std_back = []

dwppodd_snr = []
dwppbitt_snr = []
dwplodd_snr = []
dwplbitt_snr = []
dwnoodd_snr = []
dwnobitt_snr = []

taus = np.logspace(-2, 1, 100)
sigma = 0.3
T = 0.3
reps = 10000
A = 10
c = 0.042
normed = False
for tau in taus:
    print('tau = ', tau)
    dwpp_odd = []
    dwpl_odd = []
    dwno_odd = []
    dwpp_odd_back = []
    dwpl_odd_back = []
    dwno_odd_back = []
    for i in range(reps):
        spkt1, ph1 = generate_times_opt(rate_function, max_t/2., omega_ref, sigma,
                                        A, c, max_t, delta)
        spkt2, ph2 = generate_times_opt(rate_function, max_t/2. + T, omega_ref, sigma,
                                        A, c, max_t, delta)
        spkt3, ph3 = generate_times_opt(rate_function, max_t/2., omega_ref, sigma,
                                        A, 0, max_t, delta)
        spkt4, ph4 = generate_times_opt(rate_function, max_t/2. + T, omega_ref, sigma,
                                        A, 0, max_t, delta)
        spkt5, ph5 = generate_times_opt(rate_function2, max_t/2., omega_ref, sigma,
                                        A, 0, max_t, delta)
        spkt6, ph6 = generate_times_opt(rate_function2, max_t/2. + T, omega_ref, sigma,
                                        A, 0, max_t, delta)

        dwpp_odd.append(overall_w_change(spkt1, spkt2, tau=tau, window='odd', normed=normed))
        dwpp_odd_back.append(overall_w_change(spkt2, spkt1, tau=tau, window='odd', normed=normed))
        dwpl_odd.append(overall_w_change(spkt3, spkt4, tau=tau, window='odd', normed=normed))
        dwpl_odd_back.append(overall_w_change(spkt4, spkt3, tau=tau, window='odd', normed=normed))
        dwno_odd.append(overall_w_change(spkt5, spkt6, tau=tau, window='odd', normed=normed))
        dwno_odd_back.append(overall_w_change(spkt6, spkt5, tau=tau, window='odd', normed=normed))

    dwdppodd_mean.append(np.mean(np.array(dwpp_odd)-np.array(dwpp_odd_back)))
    dwdplodd_mean.append(np.mean(np.array(dwpl_odd)-np.array(dwpl_odd_back)))
    dwdnoodd_mean.append(np.mean(np.array(dwno_odd)-np.array(dwno_odd_back)))

    dwdppodd_std.append(np.std(np.array(dwpp_odd)-np.array(dwpp_odd_back)))
    dwdplodd_std.append(np.std(np.array(dwpl_odd)-np.array(dwpl_odd_back)))
    dwdnoodd_std.append(np.std(np.array(dwno_odd)-np.array(dwno_odd_back)))

    dwppodd_snr.append((np.mean(np.array(dwpp_odd)-np.array(dwpp_odd_back)))\
                       /((np.std(np.array(dwpp_odd))+np.std(np.array(dwpp_odd_back)))))
    dwplodd_snr.append((np.mean(np.array(dwpl_odd)-np.array(dwpl_odd_back)))\
                       /(np.std(np.array(dwpl_odd))+np.std(np.array(dwpl_odd_back))))
    dwnoodd_snr.append((np.mean(np.array(dwno_odd)-np.array(dwno_odd_back)))\
                       /(np.std(np.array(dwno_odd))+np.std(np.array(dwno_odd_back))))

plt.figure()
plt.plot(taus, dwdppodd_mean, 'b')
plt.plot(taus, dwdplodd_mean, 'r')
plt.plot(taus, dwdnoodd_mean, 'c')
plt.xscale("log")#, nonposx='clip')
plt.xlabel('Time constant of the STDP window (s)')
plt.ylabel('Relative synaptic change (a.u.)')
plt.xlim([0, 1.5])

"""
# store data for later use - files attached
data = np.array([taus, dwdppodd_mean,dwdplodd_mean,dwdnoodd_mean, dwdppodd_std,
                 dwdplodd_std, dwdnoodd_std, dwppodd_snr, dwplodd_snr, dwnoodd_snr]).T
header = "tau, phase precession mean, phase locking mean, no theta mean,
 phase precession std, phase locking std, no theta std, phase precession snr,
 phase locking snr, no theta snr"
with open('snr_data.txt', 'a') as f:
    f.write(header + "\n")
    f.close

with open('snr_data.txt', 'a') as f:
    for x in data:
        for elem in x:
            f.write(str(elem)+', ')
        f.write(' \n')
"""

#%%
# SNR for some experimental windows - simulations for Figure 4C
sigma = 0.3
T = 0.3
A = 10
c = 0.042
normed = False

dwpp_triplet = []
dwpp_triplet_back = []
dwpp_bipoo = []
dwpp_bipoo_back = []
dwpp_bipoo_odd = []
dwpp_bipoo_odd_back = []
dwpp_bittner = []
dwpp_bittner_back = []
dwpp_witt = []
dwpp_witt_back = []
dwpp_fro = []
dwpp_fro_back = []

dwpl_triplet = []
dwpl_triplet_back = []
dwpl_bipoo = []
dwpl_bipoo_back = []
dwpl_bipoo_odd = []
dwpl_bipoo_odd_back = []
dwpl_bittner = []
dwpl_bittner_back = []
dwpl_witt = []
dwpl_witt_back = []
dwpl_fro = []
dwpl_fro_back = []

dwno_triplet = []
dwno_triplet_back = []
dwno_bipoo = []
dwno_bipoo_back = []
dwno_bipoo_odd = []
dwno_bipoo_odd_back = []
dwno_bittner = []
dwno_bittner_back = []
dwno_witt = []
dwno_witt_back = []
dwno_fro = []
dwno_fro_back = []

for i in range(100000):
    print(i)
    spkt1, ph1 = generate_times_opt(rate_function, max_t/2., omega_ref, sigma, A, c, max_t, delta)
    spkt2, ph2 = generate_times_opt(rate_function, max_t/2. + T, omega_ref, sigma,
                                    A, c, max_t, delta)
    spkt3, ph3 = generate_times_opt(rate_function, max_t/2., omega_ref, sigma, A, 0, max_t, delta)
    spkt4, ph4 = generate_times_opt(rate_function, max_t/2. + T, omega_ref, sigma,
                                    A, 0, max_t, delta)
    spkt5, ph5 = generate_times_opt(rate_function2, max_t/2., omega_ref, sigma, A, 0, max_t, delta)
    spkt6, ph6 = generate_times_opt(rate_function2, max_t/2. + T, omega_ref, sigma,
                                    A, 0, max_t, delta)

    dwpp_triplet.append(overall_w_change(spkt1, spkt2, window='triplet', tau=0.01, normed=normed))
    dwpp_triplet_back.append(overall_w_change(spkt2, spkt1, window='triplet',
                                              tau=0.01, normed=normed))
    dwpp_bipoo.append(overall_w_change(spkt1, spkt2, tau=0.05, window='bipoo', normed=normed))
    dwpp_bipoo_back.append(overall_w_change(spkt2, spkt1, tau=0.05, window='bipoo', normed=normed))
    dwpp_bipoo_odd.append(overall_w_change(spkt1, spkt2, tau=0.05, window='bipoo_odd',
                                           normed=normed))
    dwpp_bipoo_odd_back.append(overall_w_change(spkt2, spkt1, tau=0.05,
                                                window='bipoo_odd', normed=normed))
    dwpp_bittner.append(overall_w_change(spkt1, spkt2, tau=1., window='bittner', normed=normed))
    dwpp_bittner_back.append(overall_w_change(spkt2, spkt1, tau=1.,
                                              window='bittner', normed=normed))
    dwpp_witt.append(overall_w_change(spkt1, spkt2, tau=0.05, window='wittenberg06', normed=normed))
    dwpp_witt_back.append(overall_w_change(spkt2, spkt1, tau=0.05,
                                           window='wittenberg06', normed=normed))
    dwpp_fro.append(overall_w_change(spkt1, spkt2, tau=0.1, window='froemke05', normed=normed))
    dwpp_fro_back.append(overall_w_change(spkt2, spkt1, tau=0.1, window='froemke05', normed=normed))

    dwpl_triplet.append(overall_w_change(spkt3, spkt4, window='triplet', tau=0.01, normed=normed))
    dwpl_triplet_back.append(overall_w_change(spkt4, spkt3, window='triplet',
                                              tau=0.01, normed=normed))
    dwpl_bipoo.append(overall_w_change(spkt3, spkt4, tau=0.05, window='bipoo', normed=normed))
    dwpl_bipoo_back.append(overall_w_change(spkt4, spkt3, tau=0.05, window='bipoo', normed=normed))
    dwpl_bipoo_odd.append(overall_w_change(spkt3, spkt4, tau=0.05,
                                           window='bipoo_odd', normed=normed))
    dwpl_bipoo_odd_back.append(overall_w_change(spkt4, spkt3, tau=0.05,
                                                window='bipoo_odd', normed=normed))
    dwpl_bittner.append(overall_w_change(spkt3, spkt4, tau=1., window='bittner', normed=normed))
    dwpl_bittner_back.append(overall_w_change(spkt4, spkt3, tau=1.,
                                              window='bittner', normed=normed))
    dwpl_witt.append(overall_w_change(spkt3, spkt4, tau=0.05, window='wittenberg06', normed=normed))
    dwpl_witt_back.append(overall_w_change(spkt4, spkt3, tau=0.05,
                                           window='wittenberg06', normed=normed))
    dwpl_fro.append(overall_w_change(spkt3, spkt4, tau=0.1, window='froemke05', normed=normed))
    dwpl_fro_back.append(overall_w_change(spkt4, spkt3, tau=0.1, window='froemke05', normed=normed))

    dwno_triplet.append(overall_w_change(spkt5, spkt6, window='triplet', tau=0.01, normed=normed))
    dwno_triplet_back.append(overall_w_change(spkt6, spkt5, window='triplet',
                                              tau=0.01, normed=normed))
    dwno_bipoo.append(overall_w_change(spkt5, spkt6, tau=0.05, window='bipoo', normed=normed))
    dwno_bipoo_back.append(overall_w_change(spkt6, spkt5, tau=0.05, window='bipoo', normed=normed))
    dwno_bipoo_odd.append(overall_w_change(spkt5, spkt6, tau=0.05,
                                           window='bipoo_odd', normed=normed))
    dwno_bipoo_odd_back.append(overall_w_change(spkt6, spkt5, tau=0.05,
                                                window='bipoo_odd', normed=normed))
    dwno_bittner.append(overall_w_change(spkt5, spkt6, tau=1., window='bittner', normed=normed))
    dwno_bittner_back.append(overall_w_change(spkt6, spkt5, tau=1.,
                                              window='bittner', normed=normed))
    dwno_witt.append(overall_w_change(spkt5, spkt6, tau=0.05, window='wittenberg06', normed=normed))
    dwno_witt_back.append(overall_w_change(spkt6, spkt5, tau=0.05,
                                           window='wittenberg06', normed=normed))
    dwno_fro.append(overall_w_change(spkt5, spkt6, tau=0.1, window='froemke05', normed=normed))
    dwno_fro_back.append(overall_w_change(spkt6, spkt5, tau=0.1, window='froemke05', normed=normed))

snr_pp_triplet = (np.mean(np.array(dwpp_triplet))-np.mean(np.array(dwpp_triplet_back)))\
    /(np.std(np.array(dwpp_triplet)) + np.std(np.array(dwpp_triplet_back)))
snr_pp_bipoo = (np.mean(np.array(dwpp_bipoo))-np.mean(np.array(dwpp_bipoo_back)))\
    /(np.std(np.array(dwpp_bipoo)) + np.std(np.array(dwpp_bipoo_back)))
snr_pp_bipoo_odd = (np.mean(np.array(dwpp_bipoo_odd)-np.array(dwpp_bipoo_odd_back)))\
    /(np.std(np.array(dwpp_bipoo_odd)) + np.std(np.array(dwpp_bipoo_odd_back)))
snr_pp_bittner = (np.mean(np.array(dwpp_bittner)-np.array(dwpp_bittner_back)))\
    /(np.std(np.array(dwpp_bittner)) + np.std(np.array(dwpp_bittner_back)))
snr_pp_witt = (np.mean(np.array(dwpp_witt)-np.array(dwpp_witt_back)))\
    /(np.std(np.array(dwpp_witt)) + np.std(np.array(dwpp_witt_back)))
snr_pp_fro = (np.mean(np.array(dwpp_fro)-np.array(dwpp_fro_back)))\
    /(np.std(np.array(dwpp_fro)) + np.std(np.array(dwpp_fro_back)))

snr_pl_triplet = (np.mean(np.array(dwpl_triplet))-np.mean(np.array(dwpl_triplet_back)))\
    /(np.std(np.array(dwpl_triplet)) + np.std(np.array(dwpl_triplet_back)))
snr_pl_bipoo = (np.mean(np.array(dwpl_bipoo))-np.mean(np.array(dwpl_bipoo_back)))\
    /(np.std(np.array(dwpl_bipoo)) + np.std(np.array(dwpl_bipoo_back)))
snr_pl_bipoo_odd = (np.mean(np.array(dwpl_bipoo_odd)-np.array(dwpl_bipoo_odd_back)))\
    /(np.std(np.array(dwpl_bipoo_odd)) + np.std(np.array(dwpl_bipoo_odd_back)))
snr_pl_bittner = (np.mean(np.array(dwpl_bittner)-np.array(dwpl_bittner_back)))\
    /(np.std(np.array(dwpl_bittner)) + np.std(np.array(dwpl_bittner_back)))
snr_pl_witt = (np.mean(np.array(dwpl_witt)-np.array(dwpl_witt_back)))\
    /(np.std(np.array(dwpl_witt)) + np.std(np.array(dwpl_witt_back)))
snr_pl_fro = (np.mean(np.array(dwpl_fro)-np.array(dwpl_fro_back)))\
    /(np.std(np.array(dwpl_fro)) + np.std(np.array(dwpl_fro_back)))

snr_no_triplet = (np.mean(np.array(dwno_triplet))-np.mean(np.array(dwno_triplet_back)))\
    /(np.std(np.array(dwno_triplet)) + np.std(np.array(dwno_triplet_back)))
snr_no_bipoo = (np.mean(np.array(dwno_bipoo))-np.mean(np.array(dwno_bipoo_back)))\
    /(np.std(np.array(dwno_bipoo)) + np.std(np.array(dwno_bipoo_back)))
snr_no_bipoo_odd = (np.mean(np.array(dwno_bipoo_odd)-np.array(dwno_bipoo_odd_back)))\
    /(np.std(np.array(dwno_bipoo_odd)) + np.std(np.array(dwno_bipoo_odd_back)))
snr_no_bittner = (np.mean(np.array(dwno_bittner)-np.array(dwno_bittner_back)))\
    /(np.std(np.array(dwno_bittner)) + np.std(np.array(dwno_bittner_back)))
snr_no_witt = (np.mean(np.array(dwno_witt)-np.array(dwno_witt_back)))\
    /(np.std(np.array(dwno_witt)) + np.std(np.array(dwno_witt_back)))
snr_no_fro = (np.mean(np.array(dwno_fro)-np.array(dwno_fro_back)))\
    /(np.std(np.array(dwno_fro)) + np.std(np.array(dwno_fro_back)))

"""
# write data to file - files attached
data = np.array([snr_pp_triplet, snr_pp_bipoo, snr_pp_bipoo_odd, snr_pp_bittner,
                 snr_pp_witt, snr_pp_fro, snr_pl_triplet, snr_pl_bipoo,
                 snr_pl_bipoo_odd, snr_pl_bittner, snr_pl_witt, snr_pl_fro,
                 snr_no_triplet, snr_no_bipoo, snr_no_bipoo_odd, snr_no_bittner,
                 snr_no_witt, snr_no_fro]).T
with open('expwindows_snr.txt', 'a') as f:
    for x in data:
        f.write(str(x)+'\n')
"""
#%%
# Figure 4C including SNR for experimental windows

data = np.zeros((100, 10))
f = open("snr_data.txt", "r")
for i, x in enumerate(f):
    if x[0] != 't':
        data[i-1, :] = np.array([float(elem) for elem in x.split(',')[:-1]])
f.close()
taus, dwdppodd_mean, dwdplodd_mean, dwdnoodd_mean, dwdppodd_std, dwdplodd_std,\
 dwdnoodd_std, dwppodd_snr, dwplodd_snr, dwnoodd_snr = data.T

data2 = np.zeros(18)
f = open("expwindows_snr.txt", "r")
for i, x in enumerate(f):
    print(i)
    data2[i] = float(x)
f.close()
snr_pp_triplet, snr_pp_bipoo, snr_pp_bipoo_odd, snr_pp_bittner, snr_pp_witt,\
snr_pp_fro, snr_pl_triplet, snr_pl_bipoo, snr_pl_bipoo_odd, snr_pl_bittner,\
snr_pl_witt, snr_pl_fro, snr_no_triplet, snr_no_bipoo, snr_no_bipoo_odd,\
snr_no_bittner, snr_no_witt, snr_no_fro = data2.T

plt.figure(figsize=(10, 5))
plt.plot(taus, dwppodd_snr, 'b', linewidth=2)
#plt.plot(taus, dwppbitt_snr, 'k')
#plt.plot(taus, dwppeven_snr, 'g')
plt.plot(taus, dwnoodd_snr, 'c', linewidth=2)
plt.plot(taus, dwplodd_snr, 'r', linewidth=2)
plt.plot(taus, dwppodd_snr, 'b', linewidth=2)
#plt.plot(taus, dwplbitt_snr, 'c')
#plt.plot(taus, dwpleven_snr, 'm')
#plt.plot(taus, np.ones(len(taus)), 'k--')
plt.plot(taus, 1.58 * np.ones(len(taus)), 'k--')
plt.plot([1, 1], [snr_pl_bittner, snr_pp_bittner], 'k-')
plt.scatter([1], [snr_no_bittner], s=100, c='c', zorder=10)
plt.scatter([1], [snr_pl_bittner], s=100, c='r', zorder=10)
plt.scatter([1], [snr_pp_bittner], s=100, c='b', zorder=10)
plt.plot([0.025, 0.025], [snr_pl_bipoo, snr_pp_bipoo], 'k-')
plt.scatter([0.025], [snr_no_bipoo], s=100, c='c', zorder=10)
plt.scatter([0.025], [snr_pl_bipoo], s=100, c='r', zorder=10)
plt.scatter([0.025], [snr_pp_bipoo], s=100, c='b', zorder=10)
#plt.scatter([0.025], [snr_pp_bipoo_odd], s=100, c='g')
#plt.scatter([0.025], [snr_pl_bipoo_odd], s=100, c='g')
plt.plot([0.03, 0.03], [snr_pl_witt, snr_pp_witt], 'k-')
plt.scatter([0.03], [snr_no_witt], s=100, c='c', zorder=10)
plt.scatter([0.03], [snr_pp_witt], s=100, c='b', zorder=10)
plt.scatter([0.03], [snr_pl_witt], s=100, c='r', zorder=10)
plt.plot([0.055, 0.055], [snr_pl_fro, snr_pp_fro], 'k-')
plt.scatter([0.055], [snr_no_fro], s=100, c='c', zorder=10)
plt.scatter([0.055], [snr_pp_fro], s=100, c='b', zorder=10)
plt.scatter([0.055], [snr_pl_fro], s=100, c='r', zorder=10)
plt.plot([0.04, 0.04], [snr_pp_triplet, snr_pl_triplet], 'k')
plt.scatter([0.04], [snr_no_triplet], s=100, c='c', zorder=10)
plt.scatter([0.04], [snr_pp_triplet], s=100, c='b', zorder=10)
plt.scatter([0.04], [snr_pl_triplet], s=100, c='r', zorder=10)
plt.xscale("log")#, nonposx='clip')
plt.yscale("log")#, nonposx='clip')
plt.legend(['Phase precession', 'Phase locking', 'No theta'])
plt.xlabel('Time constant of the STDP window (s)')
plt.ylabel('SNR')
plt.text(0.012, 0.17, 'Bi and Poo\n  (1998)')
plt.text(0.072, 0.37, 'Froemke et al. (2005)')
plt.text(0.035, 0.03, 'Wittenberg and Wang (2006)')
plt.text(1.2, 0.14, 'Bittner et al. (2017)')
plt.text(0.045, 0.17, 'Minimal triplet model, Pfister and Gerstner (2006)')
plt.xlim([0.01, 10])

#%%
# plots for Figure 4
plt.figure()
plt.subplot(3, 1, 1)
plt.plot(taus, dwdppodd_mean, lw=2, color='blue')
plt.plot(taus, dwdplodd_mean, lw=2, color='red')
#plt.plot(taus, dwdnoodd_mean, lw=2, color = 'cyan')
plt.xscale("log")#, nonposx='clip')
plt.yscale("log")#, nonposx='clip')
plt.ylabel('weight change', fontsize=20)
plt.yticks(fontsize=20)
plt.xlim([0.01, 1])
plt.subplot(3, 1, 2)
plt.plot(taus, np.array(dwdppodd_mean)/np.array(dwdplodd_mean) - 1, lw=2, color='black')
plt.xscale("log")#, nonposx='clip')
plt.ylabel('benefit', fontsize=20)
plt.xlim([0.01, 1])
plt.yticks(fontsize=20)
plt.subplot(3, 1, 3)
plt.plot(taus, dwppodd_snr, lw=2, color='blue')
plt.plot(taus, dwplodd_snr, lw=2, color='red')
#plt.scatter([1], [snr_bittner], s=100, c='k')
#plt.scatter([0.025], [snr_bipoo], s=100, c='k')
#plt.plot(taus, dwnoodd_snr, lw=2, color = 'cyan')
#plt.plot(taus, dwppbitt_snr, color='black')
plt.axhline(y=1, color='k')
plt.xscale("log")#, nonposx='clip')
plt.yscale("log")#, nonposx='clip')
plt.ylabel('SNR', fontsize=20)
plt.xlim([0.01, 1])
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.xlabel('time constant (s)', fontsize=20)
